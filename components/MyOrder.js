import React from "react";
import {TouchableOpacity, View, StyleSheet, Image, Text, FlatList, ScrollView} from 'react-native';
import MyContext from "./MyContext"
import {withNavigation} from 'react-navigation';

class MyOrder extends React.Component {

    constructor(props){
        super(props);
    }

    _renderItem = (item) => {
        if(item){
        return(
        <View style={styles.item}>
            <Text style={styles.itemName}>{item.item.food.name_ru}</Text>
            <Text style={styles.itemPrice}>{item.item.amount} шт</Text>
            <Text style={styles.itemPrice}>{item.item.price} сум</Text>
        </View>
        )
        }
    };
    static contextType = MyContext;

    render() {
        if(this.props.restaurant){
            return (
                <TouchableOpacity style={styles.wrapper} onPress={()=>this.props.navigation.navigate('Order',{
                    id: this.props.id
                })}>
                    <View style={styles.details}>
                        <View style={styles.nameBox}>
                            <Image source={{uri:this.props.restaurant.image}} style={styles.image}/>
                            <Text style={styles.name}>{this.props.restaurant.name}</Text>
                            <Text style={styles.number}>№{this.props.id} {this.props.created_at}</Text>
                        </View>
                        <FlatList
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.items}
                            data={this.props.order_food}
                            renderItem={this._renderItem}
                        />
                        <View style={styles.priceBox}>
                            <Text style={styles.status}>
                                {this.props.status == -1 ? `Отменен `: ''}
                                {this.props.status == 0 ? `В корзине `: ''}
                                {this.props.status == 1 ? `Оформлен `: ''}
                                {this.props.status == 2 ? `Принят`: ''}
                                {this.props.status == 3 ? `В пути `: ''}
                                {this.props.status == 4 ? `Доставлен `: ''}
                            </Text>
                            <Text style={styles.price}>{this.props.total_price} сум</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            );
        }else{
            return null;
        }
    }
};

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        elevation: 5,
        margin: 16,
        marginBottom: 0,
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2
    },
    image:{
        height: 50,
        width: 50,
        borderRadius: 25,
    },
    details:{
        flex: 1,
        position: 'relative',
        backgroundColor:'white',        
        borderRadius:20,
        flexDirection: 'column',
        justifyContent: 'space-between',
        elevation: 5,
        shadowColor: '#000000',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.1        
    },
    nameBox:{
        padding: 12,
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    name:{
        flex: 1,
        paddingVertical: 18,
        marginLeft: 8,
        justifyContent: 'center',
        letterSpacing: 2
    },
    number:{
        fontSize: 12,
    },
    priceBox:{
        paddingHorizontal:20,
        paddingVertical:20,        
        borderTopWidth: 1,
        borderTopColor:'rgba(0,0,0,.1)',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    price:{
        fontSize: 16,
        textAlign: 'right',
        justifyContent: 'flex-end',
        flex: 1,
    },
    status:{
        fontSize: 16,
        flex: 1,
    },
    items:{
        padding: 12,
    },
    item:{
        paddingVertical: 4,
        flex: 1,
        flexDirection: 'row'
    },
    itemName:{
        flex: 1,
    },
    itemPrice:{
        paddingHorizontal: 8,
    }
});

export default withNavigation(MyOrder);
