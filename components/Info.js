import React, { Component } from 'react'
import { Text, View , Image } from 'react-native'
import SvgUri from 'react-native-svg-uri';

export default class Info extends Component {
    render() {
        return (
            <View style={{
                height:220,
                width:180,marginLeft:20,
                borderRadius:40,
                backgroundColor:this.props.color,
                alignItems:'center',
                textAlign:'center',
                shadowColor: this.props.color,
                shadowOffset: {
                    width: 0,
                    height: 5,
                },
                shadowOpacity: 0.5,
                shadowRadius: 20,
                elevation: 10,
                marginTop:10,
                marginBottom:20
                }}>
                <View style={{flex:2,alignItems:'center',paddingTop:10}}>
                    <Image 
                    source={this.props.imageUri}
                    style={{width:80,height:80}}
                    />
                </View>
                <View style={{flex:1,paddingTop:30}}>
                    <Text style={{textAlign:'center',color:'white'}}>
                        {this.props.name}
                    </Text>
                </View>
            </View>            
        )
    }
}
