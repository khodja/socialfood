/* eslint-disable */
import React from 'react';
import Svg, {Circle, Defs, G, Line, LinearGradient, Path, Ellipse , Polygon, Rect, Stop, Use} from 'react-native-svg';

export default {
    back:   <Svg width="375" height="211" viewBox="0 0 375 211" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M0 0L375 0V166H0L0 0Z" fill="#0B2030"/>
            <Ellipse cx="179.5" cy="154.5" rx="210.5" ry="56.5" fill="#0B2030"/>
            </Svg> ,
backWhite:   <Svg width="375" height="211" viewBox="0 0 375 211" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path d="M-1 0L374 0V166H-1L-1 0Z" fill="#FFFFFF"/>
                <Ellipse cx="178.5" cy="154.5" rx="210.5" ry="56.5" fill="#FFFFFF"/>
                </Svg>,           
}
