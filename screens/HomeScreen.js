import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    TouchableOpacity,
    Animated
} from "react-native";
import Razion from '../components/Razion'
import Restaurant from '../components/Restaurant'
import AdressBar from '../components/AdressBar'
import Swiper from 'react-native-swiper'
import MyContext from "../components/MyContext";


const {height, width} = Dimensions.get('window')
export default class HomeScreen extends Component {
    static contextType = MyContext;

    constructor(props){
        super(props);

        this.state = {
            isLoaded: false,
            categories: null,
            restaurants: null,
            slider: null,
        }
    }
    componentDidMount() {
        this.getSlider();
        this.getRestaurant();
        this.getCategorie();
    }


    getCategorie = () => {
        fetch("http://165.22.161.165/api/categories")
            .then(response => response.json())
            .then(
                (result) => {
                    this.setState({
                        categories: result
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    };
    getRestaurant = () => {
        fetch("http://165.22.161.165/api/restaurant")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        restaurants: result
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    };
    getSlider = () => {
        fetch("http://165.22.161.165/api/promo")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        slider: result
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    };
    render() {
        if(this.state.slider){
        return (
            <SafeAreaView style={{ flex: 1 ,backgroundColor:'#ddd'}}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <ScrollView scrollEventThrottle={16}
                    >
                    {/* <View style={{
                        flexDirection:'column',alignItems:'center'}}>
                        <Image style={{width:60,resizeMode:'contain'}}
                        source={require('../assets/social.png')}  />
                    </View> */}
                    <AdressBar navigation={this.props.navigation} />
                            <View style={{flex:1, 
                                backgroundColor:'white',
                                paddingTop:20,
                                marginTop:10,
                                borderTopRightRadius:40,
                                borderTopLeftRadius:40,
                                // shadowColor: "blue",
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 20,
                                // },
                                // shadowOpacity: 0.1,
                                // shadowRadius: 3,
                                // elevation: 5,                                
                                }}>
                                <View style={{paddingBottom:20}}>
                                    <Text style={{
                                        marginLeft:20,
                                        fontSize:20,
                                        marginBottom:15,
                                        fontWeight:'500'}}>
                                        Акции
                                    </Text>
                                    <View style={{alignItems:'center',justifyContent:'center'}}>
                                        <Swiper style={{width:width}} height={220} autoplay
                                        paginationStyle={{
                                        alignItems:'center',
                                        bottom:-25,
                                        justifyContent:'center'}}
                                        dotColor={'rgba(11, 32, 48, .4)'}
                                        activeDotColor={'red'}
                                        >
                                            {this.state.slider ? this.state.slider.map(item => (
                                                <Image source={{uri:`${item.image}`}} 
                                                style={styles.slideImage} 
                                                key={item.id} />
                                            )) : null}
                                        </Swiper>
                                    </View>
                                </View>
                                <View style={{paddingTop:10,alignItems:'flex-end'}}>
                                    <Razion />
                                </View>
                            </View>
                            <View style={{
                                backgroundColor:'white',
                                }}>
                                <Text style={{
                                    paddingTop:20,
                                    paddingHorizontal:20,                                    
                                    fontSize:20}}>
                                    Предложения для Вас
                                </Text>
                                <View style={{
                                    marginTop:20,
                                    flexDirection:'row',flexWrap:'wrap',justifyContent:'center'}}>
                                    {this.state.restaurants ? this.state.restaurants.map(item => (
                                        <Restaurant {...item} key={item.id} width={width} navigation={this.props.navigation} />
                                    )) : null}
                                </View>
                            </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
    else{
        return this.context.getSpinner();
    }    
    }
}

const styles = StyleSheet.create({
    slideImage: {
        height:100+'%',
        width: width - 30,
        alignSelf:'center',
        borderRadius:10,
      }
  })
