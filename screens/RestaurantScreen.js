import React, { Component } from 'react'
import { Text, View , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Food from '../components/Food'
import Img from '../import/Img';
import MyContext from "../components/MyContext";
import axios from "axios";

export default class RestaurantScreen extends Component {
    static contextType = MyContext;

    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };
    constructor(props){
        super(props);

        this.state = {
            isFav: false,
            favoriteId: 0,
            currentCat: 0,
            isLoaded: false,
            restaurant: [],
            foods: [],
            long: null,
            lat: null,
            distance: null,
            day: null,
        }
    }
    componentDidMount = () => {
        this.getData();
        this.setInterval();
    };
    setInterval = () =>{
        setTimeout(() => this.setState({isLoaded: true}),1000);
    };
    getData = () => {
        const { navigation } = this.props;
        let now = new Date();
        let days = [6,0,1,2,3,4,5];
        let day = days[now.getDay()];
        axios
            .get(`http://165.22.161.165/api/restaurant/${navigation.getParam('alias')}`)
            .then(result => {
                    this.setState({
                        restaurant: result.data,
                        foods: result.data.foods,
                        day: day,
                        long: this.props.long,
                        lat: this.props.lat,
                    });
                },
                (error) => {
                    console.warn(error)
                }
            );
    };
    render() {
        if(this.state.restaurant && this.state.isLoaded){
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'rgba(226,230,239,0.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <ScrollView scrollEventThrottle={16} showsVerticalScrollIndicator={false}>
                        <View style={{position:'absolute',top:-50,left:0,right:0}}>
                            <Img name="back" width={'100%'} height={250} viewBox="0 0 350 160" />
                        </View>
                    <View style={{flex:1}}>
                        <View style={{
                            paddingTop:90,
                            paddingHorizontal:20,
                            flexDirection:'row',    
                            marginBottom:20,
                            height:150}}>
                            <View style={{flex:1}}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#ddd',letterSpacing:2,fontWeight:'bold'}}>
                                {this.state.restaurant.name}
                            </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Icon name="sliders-h" size={22} color={'#ddd'} />
                            </View>
                        </View>
                        <View style={{
                            flex:1,
                            justifyContent:'center',
                        }}>
                        {this.state.foods ? this.state.foods.map(item => (
                            <Food {...item} key={item.id} alias={this.props.navigation.getParam('alias')} navigation={this.props.navigation} />
                        )) : null}
                        </View>
                    </View>                        
                    </ScrollView>
                </View>
            </SafeAreaView>            
        )
        }
        else{
            return this.context.getSpinner();            
        }
    }
}
