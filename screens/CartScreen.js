import React, { Component } from 'react'
import { Text, View, StyleSheet , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import MyContext from '../components/MyContext'
import CartItem from '../components/CartItem'
import Img from '../import/Img';
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

export default class CartScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
        }
    };
    static contextType = MyContext;
    constructor(props){
        super(props);

        this.state = {
            redirect: 'basket',
            basket: null,
            distance: null,
            totalPrice : 0,
            moveToOrder: false
        }
    }
    componentDidMount(){
        if(this.context.token !==null){
            this.updated();
            this.props.navigation.addListener('didFocus', (route) => {
                this.updated();
            });
        }
    }
    componentDidUpdate(){
        if(this.context.token !==null){
            if(!this.state.basket){
                this.updated();
            }
        }
        else{
            this.props.navigation.navigate('Profile');
        }
    }

    updated = () => {
        axios
            .get(`http://165.22.161.165/api/order`)
            .then(result => {
                    this.setState(prevState => ({
                            ...prevState,
                            basket: result.data
                        }),()=>{
                            this.context.updateCart();
                        }
                    );
                },
                (error) => {
                    this.setState(prevState => ({
                        ...prevState,
                        basket: null
                    }));
                }
            )
    }
        
    render() {
        if(this.context.token !==null){
            if(this.state.basket){
                if(this.state.basket.restaurant_id === 0){
                    return (
                        <SafeAreaView style={{ flex: 1 , backgroundColor:'rgba(226,230,239,0.3)' }}>
                        <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                            <View style={styles.noItem}>
                                <Text style={styles.noItemText}>Ваша корзина пуста</Text>
                            </View>
                        </SafeAreaView>
                    );
                }
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'rgba(226,230,239,0.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1, }}>
                    <ScrollView scrollEventThrottle={16} showsVerticalScrollIndicator={false}>
                    <View style={{position:'absolute',top:-50,left:0,right:0}}>
                        <Img name="back" width={'100%'} height={250} viewBox="0 0 350 200" />
                    </View>
                    <View style={{flex:1}}>
                        <View style={{paddingTop:40,paddingHorizontal:20,alignItems:'center',marginBottom:50,height:120}}>
                            <View style={{flex:1}}>
                                <Text style={{textTransform:'uppercase',textAlign:'center',fontSize:20,color:'#ddd',letterSpacing:2}}>
                                    Ваша Корзина
                                </Text>
                            </View>
                            <Icon name="shopping-bag" color={'#ddd'} size={34} />
                        </View>
                        <View style={{
                            flex:1,
                            justifyContent:'center',paddingHorizontal:15,position:'relative',top:-20 }}>
                            <View style={{
                            borderRadius:30,backgroundColor:'white',
                            paddingBottom: 10,
                            paddingHorizontal: 0,
                            alignItems:'center',
                            textAlign:'center',
                            shadowColor: "blue",
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 7.5,
                            elevation: 12,
                            }}>
                                <View style={{
                                    borderTopLeftRadius:20,
                                    borderTopRightRadius:20,
                                    position:'relative',
                                    justifyContent:'center',
                                    alignItems:'center',
                                    backgroundColor:'#537cbd',
                                    height:70,width:100+'%'}}>
                                    <Text style={{textAlign:'center',lineHeight:70,color:'white'}}>
                                    {this.state.basket.restaurant.categories[0].id == 1 ? 'Заказ на '+this.context.cart.totalItems+' дня |' : null} Общая сумма : {this.context.cart.totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум
                                    </Text>
                                    {/* <View style={{
                                        position:'absolute',
                                        bottom: -40,
                                        alignSelf:'center',
                                        width:70,
                                        height:70,
                                        fontWeight:'bold'}}>
                                        <Image 
                                        style={{
                                            borderRadius:40,
                                        }}
                                        source={{uri:this.state.basket.restaurant.image,width:'100%',height:'100%'}} />
                                    </View> */}
                                </View>
                                {this.state.basket ? this.state.basket.order_food.map(item => (
                                    <CartItem {...item} key={item.id} updated={this.updated} />
                                )) : null}
                                <Text style={{fontWeight:'bold'}}>{this.state.basket.restaurant.categories[0].id == 1 ? '*Правильное питание доставят на следущее утро' : null}</Text>
                                <View style={{alignItems:'center',paddingTop:30,paddingBottom:10}}>
                                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('Review')}
                                    style={{backgroundColor:'#537cbd',borderRadius:10,paddingVertical:15,width:150}}>
                                        <Text style={{color:'white',textAlign:'center',letterSpacing:0.4,textTransform:'uppercase'}}>Оформить</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>             
                    </ScrollView>
                </View>
            </SafeAreaView>            
        );
            }
            else{
                return this.context.getSpinner();
            }
        }else{
            return (
                <View style={styles.body}>
                    <View style={styles.noItem}>
                        <Text style={styles.noItemText}>Авторизуйтесь, чтобы видеть корзину</Text>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    body: {
        position: 'relative',
        flex: 1,
    },
    details:{
        flex: 1,
        padding: 16,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    noItem:{
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    noItemIcon: {
        width: 128,
        height: 128
    },
    noItemText: {
        marginTop: 16,
        fontSize: 18,
    },
    nameBox:{
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 18
    },
    distanceBox:{
        flex: 1,
        alignItems: 'flex-end',
        paddingVertical: 10
    },
    name:{
        textTransform: 'uppercase',
        letterSpacing: 2
    },
    cat:{
        color: 'rgba(51, 51, 51, 0.5)'
    },
    logo:{
        width: 50,
        height: 50,
    },
    priceBox:{
        textAlign: 'right',
        flexDirection: 'row'
    },
    price:{
        textAlign: 'right'
    },
    distance:{
        textAlign: 'right'
    },
    truckIcon:{
        marginRight: 8
    },
    items:{
        flex: 1,
        paddingVertical: 16,
        marginBottom: 112
    },
    itemsDesc:{
        marginHorizontal: 16,
        display: 'flex',
        flexDirection: 'row',
        padding: 16
    },
    itemsDescText:{
        fontSize: 16,
        flex: 1,
    },
    itemsDescPrice:{
        flex: 1,
        textAlign: 'right',
        fontSize: 16,
    },
    addCart:{
        position: 'absolute',
        bottom: 0,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        textAlign: 'center',
        padding: 16,
    },
    addCartText:{
        fontSize: 16,
        flex: 1,
        textAlign: 'center'
    }
});
