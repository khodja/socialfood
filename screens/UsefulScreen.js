import React, { Component } from 'react'
import { Text, View , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Useful from '../components/Useful'
import Img from '../import/Img';

export default class ProfileScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Chat',
            headerTitle: 'Chat',
            headerStyle: { backgroundColor: 'red' },
            headerTitleStyle: { color: 'green' },
        }
    };    
    render() {
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#192833', borderBottomWidth: 1, borderBottomColor:'rgba(0,0,0,.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                <ScrollView scrollEventThrottle={16} >                    
                <View style={{position:'absolute',top:-70,left:0,right:0}}>
                    <Img name="backWhite" width={'100%'} height={300} viewBox="0 0 350 160" />
                </View>
                    <View style={{flex:1}}>
                        <View style={{paddingTop:90,paddingHorizontal:20,
                            flexDirection:'row',
                            alignItems:'center',
                            marginBottom:100,height:150}}>
                            <View style={{flex:1}}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#192833',letterSpacing:3,fontWeight:'bold'}}>
                                Полезное
                            </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Icon name="heart" size={30} color={'red'} />
                            </View>                            
                        </View>
                        <View style={{flex:1}}>
                            <Useful navigation={this.props.navigation}/>
                            <Useful navigation={this.props.navigation}/>
                            <Useful navigation={this.props.navigation}/>
                            <Useful navigation={this.props.navigation}/>
                            <Useful navigation={this.props.navigation}/>
                        </View>                        
                    </View>
                    </ScrollView>
                </View>
            </SafeAreaView>            
        )
    }
}
